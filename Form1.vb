Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO


Imports ArchiveConvertLibrary

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Lagre As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDato As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Lagre = New System.Windows.Forms.Button()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxDato = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(7, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Artikkel ID"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(64, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(80, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(152, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 24)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&Hente Artikkel"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(64, 124)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(560, 20)
        Me.TextBox2.TabIndex = 6
        Me.TextBox2.Text = ""
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(64, 160)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(560, 360)
        Me.TextBox4.TabIndex = 7
        Me.TextBox4.Text = ""
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(7, 126)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Tittel"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(7, 169)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 24)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Artikkel"
        '
        'Lagre
        '
        Me.Lagre.Enabled = False
        Me.Lagre.Location = New System.Drawing.Point(368, 24)
        Me.Lagre.Name = "Lagre"
        Me.Lagre.Size = New System.Drawing.Size(104, 24)
        Me.Lagre.TabIndex = 3
        Me.Lagre.Text = "&Lagre Endringer"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(64, 62)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox3.Size = New System.Drawing.Size(560, 48)
        Me.TextBox3.TabIndex = 5
        Me.TextBox3.Text = ""
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(7, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 24)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Priv-til-red"
        '
        'TextBoxDato
        '
        Me.TextBoxDato.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDato.Location = New System.Drawing.Point(488, 24)
        Me.TextBoxDato.Name = "TextBoxDato"
        Me.TextBoxDato.ReadOnly = True
        Me.TextBoxDato.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxDato.TabIndex = 4
        Me.TextBoxDato.Text = ""
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(488, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 20)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Publisert dato:"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(256, 24)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(96, 24)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Avbryt"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(64, 536)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(560, 20)
        Me.TextBox5.TabIndex = 10
        Me.TextBox5.Text = ""
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 536)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 24)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "ATekst-rettelse"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(634, 576)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label6, Me.TextBox5, Me.btnCancel, Me.TextBoxDato, Me.Label5, Me.Label3, Me.TextBox3, Me.Lagre, Me.TextBox4, Me.TextBox2, Me.Button1, Me.TextBox1, Me.Label1, Me.Label2, Me.Label4})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Editere Artikkel"
        Me.ResumeLayout(False)

    End Sub

#End Region

    'declare global variables
    Dim xmlDoc As XmlDocument = New XmlDocument()
    Dim privToRed As XmlNode
    Dim title As XmlNode
    Dim ingress As XmlNode
    Dim body As XmlNode
    Dim head As XmlNode
    Dim bodyOrig As XmlNode
    Dim refId As Integer
    Dim strConnectionString As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionString")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Prevent doing search when articleId is empty or when field doesn't contain an integer.
        Dim intTemp As Integer
        If (Me.TextBox1.Text <> "") Then
            Try
                intTemp = CInt(Me.TextBox1.Text)
            Catch
                MsgBox("Feltet 'Artikkel ID', m� inneholde en gyldig artikkel ID")
                Exit Sub
            End Try
            refId = Me.TextBox1.Text
            RunQuery()
        End If
    End Sub

    Public Sub RunQuery()
        'Dim Message As String
        Dim myConnection As SqlConnection
        Dim mySqlCommand As SqlCommand
        Dim myDataReader As SqlDataReader
        Dim strXml As String = String.Empty
        Dim strPreFill As String
        'Dim strLineBr As String

        Me.TextBox4.Text = ""

        myConnection = New SqlConnection(strConnectionString) ' "data source=DEVSERVER1;initial catalog=ntb;integrated security=SSPI;persist security info=False;user id=sa;workstation id=GX240-PORTAL-2;packet size=4096")
        mySqlCommand = New SqlCommand()
        mySqlCommand.Connection = myConnection

        mySqlCommand.CommandText = "getXml " & refId

        Try
            myConnection.Open()
            myDataReader = mySqlCommand.ExecuteReader(CommandBehavior.SingleRow)

        Catch err As Exception
            MsgBox(err.Message & vbCrLf & err.StackTrace)
            'Finally
        End Try

        Do While (myDataReader.Read())
            strXml = myDataReader.GetString(0)

        Loop
        myConnection.Close()

        'If strXml is nothing no data was retrived from database. Most likely because article id is invalid.
        If (String.IsNullOrEmpty(strXml)) Then
            MsgBox("Finner ikke �nsket artikkel. Sjekk om artikkel ID'en er gyldig.")
            Exit Sub
        End If

        Me.TextBox1.Enabled = False

        xmlDoc.LoadXml(strXml)
        privToRed = xmlDoc.SelectSingleNode("/nitf/head/docdata/ed-msg/@info")
        title = xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1")
        body = xmlDoc.SelectSingleNode("/nitf/body/body.content")
        bodyOrig = xmlDoc.SelectSingleNode("/nitf/body").Clone
        'head = xmlDoc.SelectSingleNode("/nitf/head")

        Try
            Me.TextBoxDato.Text = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-dato']/@content").InnerText
        Catch
        End Try

        'writing content to textboxes
        Dim dtDate As New DateTime()
        Dim strTmpPtr As String
        strTmpPtr = privToRed.InnerText()
        dtDate = Now
        strPreFill = "Endret " & dtDate.ToString("dd.MM.yyyy HH:mm") & "."
        If (strTmpPtr <> "") Then
            'Dim intNbr As Integer
            If (Not Trim(strTmpPtr).EndsWith(".")) Then
                strTmpPtr &= "."
            End If
            strPreFill = strTmpPtr & " " & vbCrLf & strPreFill
        End If

        Me.TextBox3.Text = strPreFill
        Me.TextBox2.Text = title.InnerText
        Dim strTemp As String = body.InnerXml.Replace("</p>", "</p>" & vbCrLf & vbCrLf)
        strTemp = strTemp.Replace("</hl2>", "</hl2>" & vbCrLf & vbCrLf)
        strTemp = strTemp.Replace("</media>", "</media>" & vbCrLf & vbCrLf)
        strTemp = strTemp.Replace("</media-caption>", "</media-caption>" & vbCrLf)
        strTemp = strTemp.Replace("<media-reference", vbCrLf & "<media-reference")
        strTemp = strTemp.Replace("<media-caption>", vbCrLf & "<media-caption>")

        Me.TextBox4.Text &= strTemp
        Me.Lagre.Enabled = True

    End Sub

    Private Sub SaveArticle(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Lagre.Click
        Dim strPrivToRed As String
        Dim strIngress As String
        Dim strTitle As String
        Dim intSound As Integer
        Dim intPicture As Integer

        'Dim strXml As String
        Dim xmlDocTmp As XmlDocument = New XmlDocument()
        Dim mySqlCommand As SqlCommand
        Dim myConnection As SqlConnection

        'Dim origTitle As XmlNode
        'Dim origIngress As XmlNode
        'Dim origBody As XmlNode

        Try
            'simple check to see if content in body is valid XML
            xmlDocTmp.LoadXml("<root>" & Me.TextBox4.Text & "</root>")

            'Updating XML title and body to reflect changes in inputfields
            privToRed.InnerText = Me.TextBox3.Text
            title.InnerText = Me.TextBox2.Text
            body.InnerXml = Me.TextBox4.Text.Replace(vbCrLf & vbCrLf, vbCrLf)

        Catch err As XmlException
            MsgBox(err.Message)
            Exit Sub
        End Try

        'Save to ATekstArkiv here
        If Me.TextBox5.Text <> "" Then

            Try
                Dim hdr As ConvertLibrary.xmlArticleHeader

                ConvertLibrary.LoadArticleHeaders(xmlDoc, hdr)
                hdr.correction = Me.TextBox5.Text

                Dim str As String = ConvertLibrary.TransformArticle(hdr)
                Dim filename As String = System.Configuration.ConfigurationManager.AppSettings("AtekstOutput") & "ntb.data-" & Now.ToString("yyyy-MM-dd-hhmm")

                Try
                    Dim aout As StreamWriter = New StreamWriter(filename, False, System.Text.Encoding.GetEncoding("iso-8859-1"))
                    aout.WriteLine(str)
                    aout.Close()
                Catch err As XmlException
                    MsgBox(err.Message)
                    Exit Sub
                End Try
            Catch ex As Exception
                MessageBox.Show("Det oppsto en feil i retting til Atekst-arkivet. Trykk OK for � fortsette rettingen i portalen.")
            End Try
        End If

        'values to store to database is refId, strTitle, strIngress, and strXml file.
        privToRed = xmlDoc.SelectSingleNode("/nitf/head/docdata/ed-msg/@info")
        title = xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1")
        ingress = xmlDoc.SelectSingleNode("/nitf/body/body.content/p[@lede='true']")

        'check to see if article contains title and ingress.
        'If no title display msg and exit sub.
        'If no inngress give strIngress empty value, else give ingress value
        'If no privToRed give strprivToRed empty value, else give privToRed value
        If (title.InnerText = "") Then
            MsgBox("Artikkelen m� ha en tittel. Denne skrives inn i tittelfeltet som tekst uten tagger.")
            Exit Sub
        End If
        If (privToRed Is Nothing) Then
            strPrivToRed = ""
        Else
            strPrivToRed = privToRed.InnerText
        End If
        If (ingress Is Nothing) Then
            strIngress = ""
        Else
            strIngress = ingress.InnerText
        End If

        strTitle = title.InnerText
        intSound = xmlDoc.SelectNodes("nitf/body/body.content/media[@media-type='audio']").Count
        intPicture = xmlDoc.SelectNodes("nitf/body/body.content/media[@media-type='image' and media-reference/@source!='']").Count

        Dim elem As XmlElement = xmlDoc.CreateElement("body_history")
        'elem.InnerText = "test" 'bodyOrig
        elem.InnerXml = "<outdated>" & Format(Now, "yyyy-MM-ddTHH:mm:ss") & "</outdated>" & bodyOrig.InnerXml

        'Add the node to the document.
        xmlDoc.SelectSingleNode("/nitf").AppendChild(elem)

        '************* remember to take this line out of the code **********
        ' xmlDoc.Save("c:\TestOut.xml")
        '************* remember to take this line out of the code **********

        Dim sw As IO.Stream = New IO.MemoryStream()
        Dim xmlTw As XmlTextWriter = New XmlTextWriter(sw, System.Text.Encoding.GetEncoding("iso-8859-1"))

        xmlTw.Indentation = 0
        xmlTw.Formatting = Formatting.Indented
        xmlDoc.Save(xmlTw)
        xmlTw.Flush()
        sw.Position = 0
        Dim sr As IO.StreamReader = New IO.StreamReader(sw, System.Text.Encoding.GetEncoding("iso-8859-1"))
        'strXml = sr.ReadToEnd

        'all checks complete, starting procedure for updating article in database.
        myConnection = New SqlConnection(strConnectionString) '("data source=DEVSERVER1;initial catalog=ntb;integrated security=SSPI;persist security info=False;user id=sa;workstation id=GX240-PORTAL-2;packet size=4096")
        myConnection.Open()

        mySqlCommand = New SqlCommand()

        mySqlCommand.CommandText = "[updateArticle]"
        mySqlCommand.CommandType = System.Data.CommandType.StoredProcedure
        mySqlCommand.Connection = myConnection
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefID", System.Data.SqlDbType.Int, 4))

        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HasPictures", System.Data.SqlDbType.TinyInt, 1))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HasSounds", System.Data.SqlDbType.TinyInt, 1))

        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleTitle", System.Data.SqlDbType.VarChar, 255))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ingress", System.Data.SqlDbType.VarChar, 1024))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticleXML", System.Data.SqlDbType.Text))

        mySqlCommand.Parameters("@RefID").Value = refId
        mySqlCommand.Parameters("@ArticleTitle").Value = strTitle
        mySqlCommand.Parameters("@Ingress").Value = strIngress
        mySqlCommand.Parameters("@HasPictures").Value = intPicture
        mySqlCommand.Parameters("@HasSounds").Value = intSound

        mySqlCommand.Parameters("@ArticleXML").Value = sr.ReadToEnd

        Try
            mySqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & vbCrLf & ex.StackTrace)
        End Try

        myConnection.Close()
        xmlTw.Close()
        sr.Close()
        sw.Close()

        'Cleraring the windows and giving a confirmation that article is updated.
        ClearText()
        MsgBox("Dine endringer er n� lagret.")

    End Sub

    Sub ClearText()
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox4.Text = ""
        Me.TextBoxDato.Text = ""
        Me.TextBox1.Enabled = True
        Me.Lagre.Enabled = False
        Me.TextBox1.Focus()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearText()
    End Sub
End Class
